<?php


    /**
    * Every time someone saves a post in wordpress, log_when_saved() is called
    */
    add_action('save_post', 'log_when_saved');
    add_filter('post_log_message', function( $message ){
        return $message. " It was saved at ".date("Y-m-d H:i:s");
    });
    function log_when_saved($post_id)
    {
        
        /*
        * runs every time post is updated or auto saved, but we didnt want that, to avoid it writing the following code,
        */
        if(! wp_is_post_revision( $post_id ) || wp_is_post_autosave($post_id))
        {
            return;
        }

        $post_log = get_stylesheet_directory(). '/post_log.txt';
        // $message = get_the_title($post_id)." was just saved!!!!!!";
        $message = apply_filters('post_log_message', get_the_title($post_id)." was just saved!");

        save_log($message, $post_log);
    }


    /**
    * Every time wordpress redirects the template, add this logic to it
    */
    add_action('template_redirect', 'members_only');
    function members_only()
    {
        
        if (is_page('super-secret-stuff') && ! is_user_logged_in()) 
        {
            
                do_action('user_redirected', date('Y-m-d H:i:s'));
                wp_redirect(home_url());
                die;
        }
    }


    add_action('user_redirected', 'log_when_accessed');
    function log_when_accessed($date = "")
    {

        $message = "somebody wanted to see my super-secret-stuff at ".$date."!!!";
        $file = get_stylesheet_directory(). '/access_log.txt';
        save_log($message, $file);
    }


    add_filter('excerpt_length', 'custom_excerpt_length');
    function custom_excerpt_length($length)
    {
        
        return 20;
    }


    /**
     * Assignment question1
     * Write a callback on shutdown hook which would print list of all callbacks hooked on init hook.
     */
    add_action('shutdown', 'print_init_hooks');
    function print_init_hooks()
    {

        $post_log = get_stylesheet_directory(). '/post_log.txt';
        // $myfile = fopen($post_log, "a+") or die("Unable to open this freaking file!!!");
        // fwrite($myfile, json_encode([$wp_filter], JSON_PRETTY_PRINT));
        // fclose($myfile);
    }

    /**
     * Assignment question1
     * Write a callback on shutdown hook which would print list of all callbacks hooked on init hook.
     */
    // add_action('admin_menu', 'remove_tool_menu');
    function remove_tool_menu()
    {

        remove_menu_page( 'tools.php' ); 
    }


    function foobar_func( $atts )
    {
        return "foo and bar";
    }
    add_shortcode( 'foobar', 'foobar_func' );
    

    function bartag_func( $atts ) 
    {

        $a = shortcode_atts( array(
            'foo' => 'something',
            'bar' => 'something else',
        ), $atts );

        return "foo : {$a['foo']} | bar : {$a['bar']}";
    }
    add_shortcode( 'bartag', 'bartag_func' );


    add_shortcode('mnet_ad', 'add_mnet_ad');
    function add_mnet_ad()
    {
        
        $script = '<script id="mNCC" language="javascript">';
        $script .= 'medianet_width = "800";';
        $script .= 'medianet_height = "250";';
        $script .= 'medianet_crid = "185897927"';
        $script .= 'medianet_versionId = "3111299";';
        $script .= '</script>';
        $script .= '<script src="https://contextual.media.net/nmedianet.js?cid=8CUY82DKD"></script>';
        return $script;
    }

    // Remove rewrite rules and then recreate rewrite rules.
    flush_rewrite_rules( false );

    function registerProductPostType() 
    {

        $labels = array(
            'name'               => _x( 'Products-mod1-name', 'post type general name' ),
            'singular_name'      => _x( 'Product-mod2-singular_name', 'post type singular name' ),
            'add_new'            => _x( 'Add New-mod3-add_new', 'book-mod3-add_new' ),
            'add_new_item'       => __( 'Add New Product-mod4-add_new_item' ),
            'edit_item'          => __( 'Edit Product-mod5-edit_item' ),
            'new_item'           => __( 'New Product-mod6-new_item' ),
            'all_items'          => __( 'All Products-mod7-all_items' ),
            'view_item'          => __( 'View Product-mod8-view_item' ),
            'search_items'       => __( 'Search Products-mod9-search_items' ),
            'not_found'          => __( 'No products found-mod10-not_found' ),
            'not_found_in_trash' => __( 'No products found in the Trash-mod11-not_found_in_trash' ), 
            'parent_item_colon'  => '',
            'menu_name'          => 'Products-mode12_menu_name'
          );
          $args = array(
            'labels'        => $labels,
            'description'   => 'Holds our products and product specific data-mod13',
            'public'        => true,
            'menu_position' => 5,
            'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ), //cant be modified
            'has_archive'   => true,
            'show_in_rest'  => false,
          );
        register_post_type( 'product', $args ); 
    }
      
    add_action( 'init', 'registerProductPostType' );


    function productPostTypeUpdatedMessages( $messages ) 
    {
        global $post, $post_ID;
        $messages['product'] = array(
            0 => '', 
            1 => sprintf( __('Product updated. <a href="%s">View product</a>'), esc_url( get_permalink($post_ID) ) ),
            2 => __('Custom field updated.'),
            3 => __('Custom field deleted.'),
            4 => __('Product updated.'),
            5 => isset($_GET['revision']) ? sprintf( __('Product restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
            6 => sprintf( __('Product published. <a href="%s">View product</a>'), esc_url( get_permalink($post_ID) ) ),
            7 => __('Product saved.'),
            8 => sprintf( __('Product submitted. <a target="_blank" href="%s">Preview product</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
            9 => sprintf( __('Product scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview product</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
            10 => sprintf( __('Product draft updated. <a target="_blank" href="%s">Preview product</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
        );
        return $messages;
    }

    add_filter( 'post_updated_messages', 'productPostTypeUpdatedMessages' );

    function productPostTypeContextualHelp() 
    { 

        $screen = get_current_screen();
    
        if ( 'product' == $screen->id ) {
            $screen->add_help_tab([
                'id' => 'products',
                'title' => 'Overview',
                'content' => '<h2>Products</h2>
                <p>Products show the details of the items that we sell on the website. You can see a list of them on this page in reverse chronological order - the latest one we added is first.</p> 
                <p>You can view/edit the details of each product by clicking on its name, or you can perform bulk actions using the dropdown menu and selecting multiple items.</p>'
            ]);
        } 
        
        if ( 'edit-product' == $screen->id ) {
            $screen->add_help_tab([
                'id' => 'editing_products',
                'title' => 'Overview',
                'content' => '<h2>Editing Products</h2>
                <p>This page allows you to view/modify product details. Please make sure to fill out the available boxes with the appropriate details (product image, price, brand) and <strong>not</strong> add these details to the product description.</p>'
            ]);
        }
      }
    
    add_action( 'admin_head', 'productPostTypeContextualHelp');

    /** 
     * TAXANOMIES
     */

    function registerProductCategoryTaxonomy() 
    {

        $labels = array(
          'name'              => _x( 'Product Categories', 'taxonomy general name' ),
          'singular_name'     => _x( 'Product Category', 'taxonomy singular name' ),
          'search_items'      => __( 'Search Product Categories' ),
          'all_items'         => __( 'All Product Categories' ),
          'parent_item'       => __( 'Parent Product Category' ),
          'parent_item_colon' => __( 'Parent Product Category:' ),
          'edit_item'         => __( 'Edit Product Category' ), 
          'update_item'       => __( 'Update Product Category' ),
          'add_new_item'      => __( 'Add New Product Category' ),
          'new_item_name'     => __( 'New Product Category' ),
          'menu_name'         => __( 'Product Categories' ),
        );
        $args = array(
          'labels' => $labels,
          'hierarchical' => true,
        );
        register_taxonomy( 'product_category', 'product', $args );
    }
      
    add_action( 'init', 'registerProductCategoryTaxonomy', 0 );


    /** 
     * CUSTOM META BOXES
     */

    // Define the box itself,
    function productPriceMetabox() 
    {
        
        add_meta_box( 
            'product_price_box',
            __( 'Product Price' ),
            'renderProductPriceMetabox',
            'product',
            'side',
            'high'
        );
    }

    // Define the content of the meta box,
    function getProductPrice($productId = null)
    {
        global $post;
        if(empty($productId)) {
            $productId = $post->ID;
        }
        return esc_attr(get_post_meta($productId, 'product_price', true));
    }

    function renderProductPriceMetabox($product)
    {

        $productPrice = getProductPrice();
        wp_nonce_field('_product_price_box_content_nonce', 'product_price_box_content_nonce');
        echo '<label for="product_price"></label>';
        echo '<input type="text" id="product_price" name="product_price" placeholder="enter a price" value="' . $productPrice . '" />';
    }

    add_action( 'add_meta_boxes', 'productPriceMetabox' );

    // Define how the data from the box is handled
    function saveProductPrice($productId)
    {
    
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }
      
        if (!wp_verify_nonce($_POST['product_price_box_content_nonce'], '_product_price_box_content_nonce')) {
            return;
        }
      
        if (!current_user_can('edit_post', $productId)) {
            return;
        }
      
        $productPrice = floatval($_POST['product_price']);
        update_post_meta($productId, 'product_price', $productPrice);
    }

    // hook is `save_post_{$post-type}`. If metabox is created for regular posts (i.e. for `post` post type), then use `save_post` hook.
    add_action('save_post_product', 'saveProductPrice');


    /**
     * Color Dropdown: Red, Blue, Yellow
     */
    function productColorMetabox() 
    {
        
        add_meta_box( 
            'product_color_box',
            __( 'Product Color' ),
            'renderProductColorMetabox',
            'product',
            'side',
            'high'
        );
    }

    // Define the content of the meta box,
    function getProductColor($productId = null)
    {
        global $post;
        if(empty($productId)) {
            $productId = $post->ID;
        }
        return esc_attr(get_post_meta($productId, 'product_color', true));
    }

    function renderProductcolorMetabox($product)
    {

        $possible_choices = array(
            'none' => 'Select a color...',
            'red' => "Red",
            'blue' => 'Blue',
            'yellow' => 'Yellow',
        );

        
        $productcolor = getProductcolor();
        wp_nonce_field('_product_color_box_content_nonce', 'product_color_box_content_nonce');

        echo '<label for="product_color"></label>';
        echo '<select id="product_color" name="product_color" />>';
        foreach($possible_choices as $value => $display_value )
        {

            echo '<option';
            if ($value == $productcolor) 
            {
                echo " selected ";
            }
            echo ' value = "'.$value.'" >';
            echo $display_value;
            echo ' </option>';
        }
        echo '</select>';
    }

    add_action( 'add_meta_boxes', 'productcolorMetabox' );

    // Define how the data from the box is handled
    function saveProductcolor($productId)
    {
    
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }
      
        if (!wp_verify_nonce($_POST['product_color_box_content_nonce'], '_product_color_box_content_nonce')) {
            return;
        }
      
        if (!current_user_can('edit_post', $productId)) {
            return;
        }
      
        $productcolor = $_POST['product_color'];
        update_post_meta($productId, 'product_color', $productcolor);
    }

    // hook is `save_post_{$post-type}`. If metabox is created for regular posts (i.e. for `post` post type), then use `save_post` hook.
    add_action('save_post_product', 'saveProductcolor');




    /**
     * utility function to save log data to the mentioned file
     * @param message string the message to be saved
     * @param file string the file whre the mentioned message needs to be saved
     * @return none
     */
    function save_log($message = "", $file = "")
    {
        
        $myfile = fopen($file, "a+") or die("Unable to open this freaking file!");
        fwrite($myfile, $message);
        fclose($myfile);
    }

    function misha_my_load_more_scripts() {
 
        global $wp_query; 
     
        // In most cases it is already included on the page and this line can be removed
        wp_enqueue_script('jquery');
     
        // register our main script but do not enqueue it yet
        wp_register_script( 'my_loadmore', get_stylesheet_directory_uri() . '/myloadmore.js', array('jquery') );
     
        // now the most interesting part
        // we have to pass parameters to myloadmore.js script but we can get the parameters values only in PHP
        // you can define variables directly in your HTML but I decided that the most proper way is wp_localize_script()
        wp_localize_script( 'my_loadmore', 'misha_loadmore_params', array(
            'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
            'posts' => json_encode( $wp_query->query_vars ), // everything about your loop is here
            'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
            'max_page' => $wp_query->max_num_pages
        ) );
     
         wp_enqueue_script( 'my_loadmore' );
    }
     
    add_action( 'wp_enqueue_scripts', 'misha_my_load_more_scripts' );

    function misha_loadmore_ajax_handler(){
 
        // prepare our arguments for the query
        $args = json_decode( stripslashes( $_POST['query'] ), true );
        $args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
        $args['post_status'] = 'publish';
     
        // it is always better to use WP_Query but not here
        query_posts( $args );
     
        if( have_posts() ) :
     
            // run the loop
            while( have_posts() ): the_post();
     
                // look into your theme code how the posts are inserted, but you can use your own HTML of course
                // do you remember? - my example is adapted for Twenty Seventeen theme
                get_template_part( 'template-parts/post/content', get_post_format() );
                // for the test purposes comment the line above and uncomment the below one
                // the_title();
     
     
            endwhile;
     
        endif;
        die; // here we exit the script and even no wp_reset_query() required!
    }
     
     
     
    add_action('wp_ajax_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_{action}
    add_action('wp_ajax_nopriv_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_nopriv_{action}

    function get_my_premium_products( $data ) 
    {

        $query = new WP_Query( array(
            'price' => $data['price'],
        ) );

        $posts = get_posts( array(
            'product_price' => $data['price'],
        ) );

        if ( empty( $posts ) ) {
            return new WP_Error( 'no_price', 'Invalid price', array( 'status' => 404 ) );
        }

        return $posts[0]->post_title;
    }

    add_action( 'rest_api_init', function () {
        register_rest_route( 'products/v1', '/premium_products/(?P<price>\d+)', array(
          'methods' => 'GET',
          'callback' => 'get_my_premium_products',
          'args' => array(
              'price' => array(
                  'validate_callback' => function($param, $request, $key) {
                      return is_numeric( $param );
                  }
              ),
          ),
        ) );
      } );