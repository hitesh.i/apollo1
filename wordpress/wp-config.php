<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'apollo1' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );


define('FS_METHOD','direct');

// Enable WP_DEBUG mode
define( 'WP_DEBUG', true );

// Enable Debug logging to the /wp-content/debug.log file
define( 'WP_DEBUG_LOG', true );

// Disable display of errors and warnings
define( 'WP_DEBUG_DISPLAY', True );
@ini_set( 'display_errors', 1 );

// Use dev versions of core JS and CSS files
define( 'SCRIPT_DEBUG', true );

define( 'SAVEQUERIES', true );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '023Au31Zay>U&2,NXI{P&h)s5n=~7LM@i0k:>fnux3Gg[SG>U0ZwmpM!i~vR/Sm9' );
define( 'SECURE_AUTH_KEY',  '48|mstfn=Dj;X>%JE4+uz<h@I,?z1Eg!.d]MmAA,Q80l^j@V,P $g(ylEX_5):y3' );
define( 'LOGGED_IN_KEY',    'E9.JtHE5Ao xT9A1Oj-{eW;.<~0(}&V6c2<-TdH<P|`tH17?))|foRC9uCHGg5D!' );
define( 'NONCE_KEY',        '.}LA_Y_RuH;cZ]1|#PE*Kj`mb-<~@n-1UEGw}6079sN%2OZAFuFszr*Vd2LvTR^.' );
define( 'AUTH_SALT',        '>hx#)9QLE-IC]g/oi(r1Fs.JYDD2.ogN9z6OE[=OcE:i~ey]I@F{1K2)[uCd E5>' );
define( 'SECURE_AUTH_SALT', 'L5pR_QW(ga[#DcliIpr.+jG):[KbZ,ix%S3({U@d`$AzOF,!/L`OyEdv{[~LJ+%4' );
define( 'LOGGED_IN_SALT',   '=B8/|Z]1;24?.H}?#<chH:N>tUTXW[lo,VLcVq>b,z@2Hr{0z)4Nx.h0o(eibb=V' );
define( 'NONCE_SALT',       '{|Q}C4Q_|ih)-4Y<V/[iG N~dcyjR-)@h:V;D^dNnX~A.x6E5,.6ZFBrE^ sU4~-' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'a1_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
