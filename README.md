# Apollo1
## Lets take off!!

Apollo1 was created to explore, experiment and fall in love with ✨WordPress✨


## Day 1

- About wordpress(History|OpenSource Contributers etc)
- Installation of all the dependancies
- handling of composer and dependancies
- Peeking into the basics: revisiting some of the conceots of PHP
- as [Guido] says, code is read much more often than it is written so lets learn about PHPDoc 

What is wordpress:

- Software used for creating and publishing a website(including e-commerce, blogs etc)
- it gives you a standard website at the start, you are able to tweak fonts, layouts etc according to your requirement without writing a single line of code using only your browser.
- you can choose from 55000+ widgets and plugins to optimise the performance of your website
- and everything is created by volunteer developers and hence its open source!
- wordpress.com: free webisite with their web-hosting which cant be commercilised and wordpress.org: self hosting, unique domain name, monetization possible

Primary type of contents in a wordpress website:
- posts: blog entries which can be published in category
- pages: part of website which doesnt change very often like about us, contact us etc

Video Tutorial: https://videos.wpbeginner.com/install-wordpress/
1. download wordpress tarball from https://wordpress.org/download/#download-install
2. create a database for your website
3. now go to http://localhost/path/to/your/wordpress
4. give the creds of the db while setting up the WP-project
5. voila! your webiste is ready
6. for tweaking fonts, adding images or any other customisation go to admin panel ie. http://localhost/path/to/your/wordpress

Grouping and describing the posts:
- Categories: used for grouping the posts, hierarchy can be created when a sub-category is added in a category
- Tags: describe the posts, like the site's index words
- You can add categories and tags in WordPress when creating or editing a post

Block editor in WordPress or Gutenberg:
- admin page > posts > add new
- what you have in front of you is an editor
- based on the concept of splitting the entire page in small blocks
- these blocks can be further styled according to your needs
- you can add,
    1. buttons in them
    2. images
    3. photo gallaries
    4. videos/audios
    5. code snippets
    etc
- custom blocks can be created by diving into tthe open source code of wordpress
- Think of them like legos, you want to make a world from building these small-small block on top of each other.

Classic editor:
- wordpress has a classic editor plugin which changes the editor in a classical form
- no major change, just the ui changes

WP User Avatar Plugin:
- lets all of your users design their own avatars

Twenty Twenty/Twenty nineteen WordPress theme:
- UI changes
- can be changed via http://localhost/mediadotnet/apollo1/wordpress/wp-admin/themes.php


## Frequent roadblocks
- while installing the plugin, it wont allow you on localhost
	1. put the following in wp-config.php,
		define('FS_METHOD','direct');
		what is does is,
		- When WordPress needs to install files (such as a plugin) it uses the get_filesystem_method() function to determine how to access the filesystem. If you don't define FS_METHOD it will choose a default for you, otherwise it will use your selection as long as it makes sense.
		- The default behavior will try to detect whether you're in an at-risk environment like the one I described above, and if it thinks you're safe it will use the 'direct' method. In this case WordPress will create the files directly through PHP, causing them to belong to the apache user (in this example). Otherwise it'll fall back to a safer method, such as prompting you for SFTP credentials and creating the files as you.
		- FS_METHOD = 'direct' asks WordPress to bypass that at-risk detection and always create files using the 'direct' method.

	2. sudo chown -R www-data:www-data /var/www/html/mediadotnet/apollo1/wordpress
		sudo chmod -R g+rwX /var/www/html/mediadotnet/apollo1/wordpress
